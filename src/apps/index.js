require("./index.scss")

import {MP} from './map.js'; // 载入百度地图
import echarts from "echarts";
import "./bmap.min.js"; //百度地图支持 echarts

import marker_icon from "leaflet_marker"
import "leaflet_marker_2x"
import "leaflet_marker_shadow"
import "leaflet_css"; 
import L from "leaflet"; 

import './leaflet-baidu.js'; //在百度地图中使用leaflet

//百度地图
MP("Rl2Oo7Z7dgGMzwOEWItgAArRQscGe3qG").then(BMap => {
    /* 地图初始化 开始 */
    var map = new BMap.Map("map-bd");          // 创建地图实例  
    var point = new BMap.Point(116.404, 39.915);  // 创建点坐标  
    map.centerAndZoom(point, 15);                 // 初始化地图 设置中心点坐标和地图级别 
    map.enableScrollWheelZoom(true);     //开启鼠标滚轮缩放

    //百度地图_echart
    let chart = echarts.init(document.getElementById('map-bd-echart'));
    let bmap = {
        center: [116.404, 39.915],
        zoom: 5,
        roam: true
    };
    
    let option = {
        bmap: bmap,
        geo: {
            map: 'bmap',
            polyline: true,
            progressiveThreshold: 500,
            progressive: 200,
            roam: true
        },
        series: []
    };

    chart.setOption(option);

    //在百度地图中使用leaflet
    var options = {
    	  crs: L.CRS.EPSGB3857,
    	  center: [23.812950, 120.506647],
    	  zoom: 13
    	};

	var baseMaps = {
	  Normal: new L.TileLayer.BaiduLayer("Normal.Map"),
	  satellite: new L.TileLayer.BaiduLayer("Satellite.Map"),
	  road: new L.TileLayer.BaiduLayer("Satellite.Road"),
	  cnormal: new L.TileLayer.BaiduLayer("CustomStyle.Map.normal"),
	  light: new L.TileLayer.BaiduLayer("CustomStyle.Map.light"),
	  dark: new L.TileLayer.BaiduLayer("CustomStyle.Map.dark"),
	  redalert: new L.TileLayer.BaiduLayer("CustomStyle.Map.redalert"),
	  googlelite: new L.TileLayer.BaiduLayer("CustomStyle.Map.googlelite"),
	  grassgreen: new L.TileLayer.BaiduLayer("CustomStyle.Map.grassgreen"),
	  midnight: new L.TileLayer.BaiduLayer("CustomStyle.Map.midnight"),
	  pink: new L.TileLayer.BaiduLayer("CustomStyle.Map.pink"),
	  darkgreen: new L.TileLayer.BaiduLayer("CustomStyle.Map.darkgreen"),
	  bluish: new L.TileLayer.BaiduLayer("CustomStyle.Map.bluish"),
	  grayscale: new L.TileLayer.BaiduLayer("CustomStyle.Map.grayscale"),
	  hardedge: new L.TileLayer.BaiduLayer("CustomStyle.Map.hardedge"),
	  mysytle: new L.TileLayer.BaiduLayer("CustomStyle.Map", {styles: 't%3Abuilding%7Ce%3Ag%7Cc%3A%23cccccc%2Ct%3Aroad%7Ce%3Aall%7Cc%3A%23999999%2Ct%3Aland%7Ce%3Aall%7Cc%3A%2376a5af%2Ct%3Agreen%7Ce%3Aall%7Cc%3A%236aa84f%2Ct%3Amanmade%7Ce%3Aall%7Cc%3A%23eeeeee%2Ct%3Aboundary%7Ce%3Aall%7Cc%3A%23444444'})
	};

	var overlayMaps = {
	};

	options.layers = [baseMaps.Normal];
	var map = L.map("map-osm", options);

	L.control.layers(baseMaps, overlayMaps).addTo(map);

});

//osm  leaflet
(function(){
	 var myIcon = L.icon({
	     iconUrl: marker_icon,
	     iconSize: [20, 20],
	     iconAnchor: [10, 20],
	});

	var map = L.map('map-osm-leaflet').setView([51.505, -0.09], 13);

	L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
	    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
	}).addTo(map);

	L.marker([51.5, -0.09],{
		icon: myIcon  //关于使用默认图标加载报错的问题https://segmentfault.com/q/1010000017168720#answerEditor
	}).addTo(map)
	    
})();



