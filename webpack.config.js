const HtmlwebpackPlugin = require('html-webpack-plugin');
const OpenBrowserPlugin = require('open-browser-webpack-plugin');
const webpack = require('webpack');
const ROOT = process.cwd();  // 根目录
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  context: path.resolve('./'),
  entry: {
    index:'./src/apps/index.js',
  },
  output: {
    filename: '[name].js',
    publicPath: "/",
    path: __dirname + '/build/',
  },
  module: {
    rules:[
      {
        test: /\.js[x]?$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['es2015']
          }
        }
      },
      {
        test: /\.(less|sass|scss|css)$/,
        use: ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: [{
                loader: 'css-loader',
              },
              {
                loader: 'less-loader'
              },
              {
                loader: 'sass-loader',
              }
            ]
          })
      },
      // {
      //   test: /\.(html)$/,
      //   use: {
      //     loader: 'html-loader',
      //     options: {
      //       // attrs: ['img:src']
      //     }
      //   }
      // },
      {
        test: /\.json$/,
        use: ['json-loader']
      }, 
      {
        test: /\.(png|jpg)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 8192
              // limit: 100
            }
          }
        ]
      },
      { 
        test: /\.(ttf|eot|svg|TTF|woff)(\?v=[0-9]\.[0-9]\.[0-9])?$/, 
        use: "url-loader?name=/resources/css/font/[name].[ext]" 
      },
    ]
  },
  plugins: [
   new HtmlwebpackPlugin({
       title: '',
       filename: 'index.html',
       template: './src/apps/index.html',
       // favicon: './favicon.ico',
       chunks:["index"]
     }),
  
    new OpenBrowserPlugin({
      url: 'http://localhost:8080/'
    }),

    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery',
      'window.$': 'jquery',
    }),

    require('autoprefixer'),
    // new ExtractTextPlugin('[name].scss')
    new ExtractTextPlugin({
      filename: 'css/[name].css?[contenthash:8]',
      allChunks: true
    })
  ],
  resolve: {
      alias: {
          '@':path.resolve(__dirname, './src'),
          'resources':path.resolve(__dirname, './src/resources'),
          'commons':path.resolve(__dirname, './src/commons'),
          leaflet_css: __dirname + "/node_modules/leaflet/dist/leaflet.css",
          leaflet_marker: __dirname + "/node_modules/leaflet/dist/images/marker-icon.png",
          leaflet_marker_2x: __dirname + "/node_modules/leaflet/dist/images/marker-icon-2x.png",
          leaflet_marker_shadow: __dirname + "/node_modules/leaflet/dist/images/marker-shadow.png",
          leaflet_js: __dirname + "/node_modules/leaflet/dist/leaflet.js",
          selectize_js: __dirname + "/node_modules/selectize.js/dist/js/standalone/selectize.js",
          selectize_css: __dirname + "/node_modules/selectize.js/dist/css/selectize.css",
      }
  },
  externals:{
      'BMap':'BMap'
  },
  devServer: {
    // contentBase: [
    //   path.join(ROOT, 'src/')
    // ],
    contentBase: "./src/apps/",
    hot: false,
    host: '0.0.0.0',
    port: 8080,
    historyApiFallback: {
      rewrites: [
        { from: /^\/$/, to: '/index.html' },
        { from: /^\/index/, to: '/index.html' },
      ]
    }
  }
};
